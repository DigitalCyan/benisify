use std::io::{Error, ErrorKind, Read, Result, stdin};
use std::collections::HashMap;
use atty::Stream;

fn main() -> Result<()> {
    let mut process_map: HashMap<&str, &str> = 
        [
        ("p", "b"),
        ("t", "d"),
        ("c", "n")
        ].iter().cloned().collect();
        
    let result = load_stdin();
    match result {
        Ok(mut data) => {
            data = String::from(data.strip_suffix("\n").unwrap());
            let data = process_string(&mut data, &mut process_map);
            println!("{}", data);
        },
        Err(_) => {
            print_help();
        }
    }

    return Ok(());
}

fn load_stdin() -> Result<String> {
    if atty::is(Stream::Stdin){
        return Err(Error::new(ErrorKind::Other, "stdin not redirected"));
    }
    let mut buffer = String::new();
    let mut stdin = stdin();
    stdin.read_to_string(&mut buffer)?;
    return Ok(buffer);
}

fn process_string(string: &mut String, map: &mut HashMap<&str, &str>) -> String {
    let mut out = string.clone();
    for key in map.keys(){
        out = out.replace(key, map.get(key).unwrap());
        out = out.replace(key.to_uppercase().as_str(), map.get(key).unwrap().to_uppercase().as_str())
    }
    return out;
}

fn print_help(){
    println!("Benisify is a CLI tool for benisifing other program\'s stdout\'s");
    println!("Usage: <some command with stdout> | benisify")
}
